package me.ankudryavtsev.yandextranslate.data.repository.translate

import io.reactivex.Observable
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.network.models.translate.response.TranslateResponse

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
interface ITranslateRepository {
    fun translate(text: String, fromLanguage: TranslateLanguage, toLanguage: TranslateLanguage): Observable<TranslateResponse>
}