package me.ankudryavtsev.yandextranslate.data.network.models.translate

import io.reactivex.Observable
import me.ankudryavtsev.yandextranslate.data.network.models.translate.response.TranslateResponse
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
interface ITranslateService {
    @POST("api/v1.5/tr.json/translate")
    fun translate(@Query("key") key: String, @Query("text") text: String, @Query("lang") lang: String): Observable<TranslateResponse>
}