package me.ankudryavtsev.yandextranslate.data.repository.translate

import io.reactivex.Observable
import me.ankudryavtsev.yandextranslate.BuildConfig
import me.ankudryavtsev.yandextranslate.data.network.models.translate.ITranslateService
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.network.models.translate.response.TranslateResponse
import javax.inject.Inject

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
class TranslateRepository @Inject constructor(private val translateService: ITranslateService) : ITranslateRepository {

    override fun translate(text: String, fromLanguage: TranslateLanguage, toLanguage: TranslateLanguage): Observable<TranslateResponse> =
            translateService.translate(BuildConfig.YANDEX_API_KEY, text, getLanguageString(fromLanguage, toLanguage))

    private fun getLanguageString(fromLanguage: TranslateLanguage, toLanguage: TranslateLanguage): String =
            "${fromLanguage.code}-${toLanguage.code}"
}