package me.ankudryavtsev.yandextranslate.data.repository.translate.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage

/**
 * @author Kudryavtsev Andrey
 * @date 10/11/2017
 */
@Entity(tableName = DbTables.TRANSLATIONS)
data class Translation(@ColumnInfo(name = "fromText") var fromText: String,
                       @ColumnInfo(name = "toText") var toText: String,
                       @ColumnInfo(name = "fromLanguage") var fromLanguage: TranslateLanguage,
                       @ColumnInfo(name = "toLanguage") var toLanguage: TranslateLanguage,
                       @PrimaryKey(autoGenerate = true) var id: Long = 0)