package me.ankudryavtsev.yandextranslate.data.repository.translate.model

import android.arch.persistence.room.TypeConverter
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage

/**
 * @author Kudryavtsev Andrey
 * @date 13/11/2017
 */
class TranslateConverter {
    @TypeConverter
    fun toTranslateLanguage(code: String): TranslateLanguage = TranslateLanguage.values().first { it.code == code }

    @TypeConverter
    fun toString(translateLanguage: TranslateLanguage): String = translateLanguage.code
}