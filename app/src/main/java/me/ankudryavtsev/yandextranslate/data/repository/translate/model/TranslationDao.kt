package me.ankudryavtsev.yandextranslate.data.repository.translate.model

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy.REPLACE
import android.arch.persistence.room.Query
import io.reactivex.Flowable

/**
 * @author Kudryavtsev Andrey
 * @date 10/11/2017
 */
@Dao
interface TranslationDao {
    @Query("SELECT * FROM " + DbTables.TRANSLATIONS)
    fun getAllTranslations(): Flowable<List<Translation>>

    @Insert(onConflict = REPLACE)
    fun addTranslation(translation: Translation)

    @Delete
    fun deleteTask(translation: Translation)
}