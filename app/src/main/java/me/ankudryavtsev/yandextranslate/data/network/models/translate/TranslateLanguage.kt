package me.ankudryavtsev.yandextranslate.data.network.models.translate

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
enum class TranslateLanguage(val code: String) {
    RU("ru"), EN("en"), ES("es")
}