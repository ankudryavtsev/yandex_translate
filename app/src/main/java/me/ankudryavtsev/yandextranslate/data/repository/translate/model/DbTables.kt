package me.ankudryavtsev.yandextranslate.data.repository.translate.model

/**
 * @author Kudryavtsev Andrey
 * @date 10/11/2017
 */
object DbTables {
    const val TRANSLATIONS = "translations"
}