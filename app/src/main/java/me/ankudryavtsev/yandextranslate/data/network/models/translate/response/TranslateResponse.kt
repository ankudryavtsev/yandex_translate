package me.ankudryavtsev.yandextranslate.data.network.models.translate.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
data class TranslateResponse(@SerializedName("code") val code: Int,
                             @SerializedName("lang") val lang: String,
                             @SerializedName("text") val text: List<String>)