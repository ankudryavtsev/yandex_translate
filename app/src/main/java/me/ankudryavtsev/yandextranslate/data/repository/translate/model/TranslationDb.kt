package me.ankudryavtsev.yandextranslate.data.repository.translate.model

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

/**
 * @author Kudryavtsev Andrey
 * @date 10/11/2017
 */
@Database(entities = arrayOf(Translation::class), version = 1, exportSchema = false)
@TypeConverters(TranslateConverter::class)
abstract class TranslationDb : RoomDatabase() {
    abstract fun translationDao(): TranslationDao
}