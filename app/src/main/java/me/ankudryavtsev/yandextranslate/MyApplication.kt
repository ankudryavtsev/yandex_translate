package me.ankudryavtsev.yandextranslate

import android.app.Application
import me.ankudryavtsev.yandextranslate.di.Injector

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.init(applicationContext)
    }
}