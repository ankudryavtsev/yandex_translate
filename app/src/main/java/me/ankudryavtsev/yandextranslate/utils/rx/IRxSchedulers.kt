package me.ankudryavtsev.yandextranslate.utils.rx

import io.reactivex.Scheduler

/**
 * @author Kudryavtsev Andrey
 * @date 11.09.17
 */
interface IRxSchedulers {
    fun getMainThreadScheduler(): Scheduler
    fun getIOScheduler(): Scheduler
    fun getComputationScheduler(): Scheduler
}