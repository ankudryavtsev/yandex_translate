package me.ankudryavtsev.yandextranslate.utils.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Kudryavtsev Andrey
 * @date 11.09.17
 */
class RxSchedulers @Inject constructor() : IRxSchedulers {
    override fun getMainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    override fun getIOScheduler(): Scheduler = Schedulers.io()

    override fun getComputationScheduler(): Scheduler = Schedulers.computation()
}