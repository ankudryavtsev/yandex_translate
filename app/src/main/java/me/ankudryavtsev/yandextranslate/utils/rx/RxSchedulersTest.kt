package me.ankudryavtsev.yandextranslate.utils.rx

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * @author Kudryavtsev Andrey
 * @date 11.09.17
 */
class RxSchedulersTest : IRxSchedulers {
    override fun getMainThreadScheduler(): Scheduler = Schedulers.trampoline()

    override fun getIOScheduler(): Scheduler = Schedulers.trampoline()

    override fun getComputationScheduler(): Scheduler = Schedulers.trampoline()
}