package me.ankudryavtsev.yandextranslate.di.main

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import me.ankudryavtsev.yandextranslate.business.translate.ITranslateInteractor
import me.ankudryavtsev.yandextranslate.business.translate.TranslateInteractor
import me.ankudryavtsev.yandextranslate.data.network.models.translate.ITranslateService
import me.ankudryavtsev.yandextranslate.data.repository.translate.ITranslateRepository
import me.ankudryavtsev.yandextranslate.data.repository.translate.TranslateRepository
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.TranslationDb
import me.ankudryavtsev.yandextranslate.di.ApplicationContext
import retrofit2.Retrofit

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
@Module
abstract class MainModule {

    @Binds
    @MainScope
    abstract fun provideTranslateRepository(translateRepository: TranslateRepository): ITranslateRepository

    @Binds
    @MainScope
    abstract fun provideTranslateInteractor(translateInteractor: TranslateInteractor): ITranslateInteractor

    @Module
    companion object {
        @JvmStatic
        @Provides
        @MainScope
        fun provideTranslateService(retrofit: Retrofit): ITranslateService = retrofit.create(ITranslateService::class.java)

        @JvmStatic
        @Provides
        @MainScope
        fun provideTranslationDb(@ApplicationContext context: Context): TranslationDb = Room.databaseBuilder(context, TranslationDb::class.java, "translation").build()
    }
}