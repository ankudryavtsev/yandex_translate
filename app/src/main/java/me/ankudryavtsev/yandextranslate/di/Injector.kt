package me.ankudryavtsev.yandextranslate.di

import android.content.Context
import me.ankudryavtsev.yandextranslate.di.main.MainComponent

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
object Injector {
    lateinit var context: Context

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent
                .builder()
                .context(context)
                .build()
    }

    val mainComponent: MainComponent by lazy {
        applicationComponent.plusMainComponent()
    }

    fun init(context: Context) {
        this.context = context
    }
}