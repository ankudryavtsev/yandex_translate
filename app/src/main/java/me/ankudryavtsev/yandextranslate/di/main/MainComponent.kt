package me.ankudryavtsev.yandextranslate.di.main

import dagger.Subcomponent
import me.ankudryavtsev.yandextranslate.presentation.main.view.MainActivity

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
@Subcomponent(modules = arrayOf(MainModule::class))
@MainScope
interface MainComponent {
    fun inject(mainActivity: MainActivity)
}