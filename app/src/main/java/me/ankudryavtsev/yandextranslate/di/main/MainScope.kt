package me.ankudryavtsev.yandextranslate.di.main

import javax.inject.Scope

/**
 * @author Kudryavtsev Andrey
 * @date 22/08/2017
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class MainScope