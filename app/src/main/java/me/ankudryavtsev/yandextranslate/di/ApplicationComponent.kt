package me.ankudryavtsev.yandextranslate.di

import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import me.ankudryavtsev.yandextranslate.di.main.MainComponent
import me.ankudryavtsev.yandextranslate.di.network.NetworkModule
import javax.inject.Singleton

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class))
interface ApplicationComponent {
    fun inject(application: Application)

    fun plusMainComponent(): MainComponent

    @Component.Builder
    interface Builder {

        fun build(): ApplicationComponent
        @BindsInstance fun context(@ApplicationContext context: Context): Builder
    }
}