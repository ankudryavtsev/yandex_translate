package me.ankudryavtsev.yandextranslate.di

import javax.inject.Qualifier

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class ApplicationContext