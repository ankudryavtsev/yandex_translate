package me.ankudryavtsev.yandextranslate.di

import dagger.Binds
import dagger.Module
import me.ankudryavtsev.yandextranslate.utils.rx.IRxSchedulers
import me.ankudryavtsev.yandextranslate.utils.rx.RxSchedulers
import javax.inject.Singleton

/**
 * Created by Andrey Kudryavtsev on 13.08.17.
 */
@Module
abstract class ApplicationModule() {
    @Binds
    @Singleton
    abstract fun provideRxSchedulers(rxSchedulers: RxSchedulers): IRxSchedulers
}