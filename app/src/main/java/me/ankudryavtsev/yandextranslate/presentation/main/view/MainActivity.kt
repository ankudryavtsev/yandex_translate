package me.ankudryavtsev.yandextranslate.presentation.main.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import me.ankudryavtsev.yandextranslate.R
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.Translation
import me.ankudryavtsev.yandextranslate.di.Injector
import me.ankudryavtsev.yandextranslate.presentation.main.presenter.MainPresenter
import javax.inject.Inject

class MainActivity : MvpAppCompatActivity(), IMainView {
    @Inject
    @InjectPresenter
    lateinit var mainPresenter: MainPresenter

    @ProvidePresenter
    fun providePresenter() = mainPresenter

    private lateinit var inputTextView: TextView
    private lateinit var resultTextView: TextView

    private lateinit var recyclerView: RecyclerView

    private lateinit var fromLanguageSpinner: Spinner
    private lateinit var toLanguageSpinner: Spinner

    private var fromLanguage: TranslateLanguage? = null
    private var toLanguage: TranslateLanguage? = null

    private var fromLanguages: Array<TranslateLanguage>? = null
    private var toLanguages: Array<TranslateLanguage>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.mainComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        inputTextView = findViewById(R.id.input_text)
        resultTextView = findViewById(R.id.result_text)
        recyclerView = findViewById(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        mainPresenter.registerInputTextChanges(inputTextView)
        setupSpinners()
    }

    private fun setupSpinners() {
        fromLanguageSpinner = findViewById(R.id.from_language)
        toLanguageSpinner = findViewById(R.id.to_language)
        fromLanguageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Empty
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val fromLanguage = fromLanguages?.get(position)
                this@MainActivity.fromLanguage = fromLanguage
                if (fromLanguage != null)
                    mainPresenter.fromLanguageSelected(fromLanguage, inputTextView.text.toString())
            }

        }
        toLanguageSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                //Empty
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val toLanguage = toLanguages?.get(position)
                this@MainActivity.toLanguage = toLanguage
                if (toLanguage != null)
                    mainPresenter.toLanguageSelected(toLanguage, inputTextView.text.toString())
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.save) {
            mainPresenter.onSaveClicked(inputTextView.text, fromLanguage, resultTextView.text, toLanguage)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showTranslationResult(text: String) {
        resultTextView.text = text
    }

    override fun showError(errorMessage: String) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
    }

    override fun setFromLanguages(translateLanguages: Array<TranslateLanguage>, initIndex: Int) {
        fromLanguages = translateLanguages
        fromLanguageSpinner.adapter = getSimpleArrayAdapter(translateLanguages)
        fromLanguageSpinner.setSelection(initIndex)
    }

    override fun setToLanguages(translateLanguages: Array<TranslateLanguage>, initIndex: Int) {
        toLanguages = translateLanguages
        toLanguageSpinner.adapter = getSimpleArrayAdapter(translateLanguages)
        toLanguageSpinner.setSelection(initIndex)
    }

    private fun getSimpleArrayAdapter(translateLanguages: Array<TranslateLanguage>): ArrayAdapter<String> {
        val arrayAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, translateLanguages.map { it.name })
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        return arrayAdapter
    }

    override fun showSavedTranslations(translationList: List<Translation>) {
        recyclerView.adapter = TranslationAdapter(translationList)
    }
}
