package me.ankudryavtsev.yandextranslate.presentation.main.presenter

import android.support.annotation.VisibleForTesting
import android.widget.TextView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.jakewharton.rxbinding2.widget.RxTextView
import me.ankudryavtsev.yandextranslate.business.translate.ITranslateInteractor
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.presentation.main.view.IMainView
import me.ankudryavtsev.yandextranslate.utils.rx.IRxSchedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
@InjectViewState
class MainPresenter @Inject constructor(private val translateInteractor: ITranslateInteractor, private val rxSchedulers: IRxSchedulers) : MvpPresenter<IMainView>() {

    private val debounceTimeout = 500L

    private var fromLanguage: TranslateLanguage = TranslateLanguage.RU
    private var toLanguage: TranslateLanguage = TranslateLanguage.EN

    override fun attachView(view: IMainView?) {
        super.attachView(view)
        val translateLanguages = TranslateLanguage.values()
        viewState.setFromLanguages(translateLanguages, 0)
        viewState.setToLanguages(translateLanguages, 1)
        translateInteractor.getSavedTranslations()
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe { translationList -> viewState.showSavedTranslations(translationList) }
    }

    fun registerInputTextChanges(inputTextView: TextView) {
        // RxBinding в активити, debounce в интерактор
        RxTextView.textChanges(inputTextView)
                .debounce(debounceTimeout, TimeUnit.MILLISECONDS)
                .flatMap { newText -> translateInteractor.translate(newText.toString(), fromLanguage, toLanguage).toObservable() }
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe({ viewState?.showTranslationResult(it.text.joinToString()) },
                        { viewState?.showError(it.toString()) })
    }

    @VisibleForTesting
    fun onTextChanged(currentText: String?) {
        translateInteractor.translate(currentText.toString(), fromLanguage, toLanguage)
                .subscribeOn(rxSchedulers.getIOScheduler())
                .observeOn(rxSchedulers.getMainThreadScheduler())
                .subscribe({ viewState?.showTranslationResult(it.text.joinToString()) },
                        { viewState?.showError(it.toString()) })
    }

    fun fromLanguageSelected(translateLanguage: TranslateLanguage, currentText: String? = null) {
        fromLanguage = translateLanguage
        if (!currentText.isNullOrEmpty())
            onTextChanged(currentText)
    }

    fun toLanguageSelected(translateLanguage: TranslateLanguage, currentText: String? = null) {
        toLanguage = translateLanguage
        if (!currentText.isNullOrEmpty())
            onTextChanged(currentText)
    }

    fun onSaveClicked(fromText: CharSequence, fromLanguage: TranslateLanguage?, toText: CharSequence, toLanguage: TranslateLanguage?) {
        if (fromLanguage != null && toLanguage != null) {
            translateInteractor.saveTranslation(fromText.toString(), fromLanguage, toText.toString(), toLanguage)
                    .andThen(translateInteractor.getSavedTranslations())
                    .subscribeOn(rxSchedulers.getIOScheduler())
                    .observeOn(rxSchedulers.getMainThreadScheduler())
                    .subscribe { translationList -> viewState.showSavedTranslations(translationList) }
        }
    }
}