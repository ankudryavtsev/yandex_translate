package me.ankudryavtsev.yandextranslate.presentation.main.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.Translation

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
interface IMainView : MvpView {
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showTranslationResult(text: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(errorMessage: String)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setFromLanguages(translateLanguages: Array<TranslateLanguage>, initIndex: Int)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setToLanguages(translateLanguages: Array<TranslateLanguage>, initIndex: Int)

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showSavedTranslations(translationList: List<Translation>)
}