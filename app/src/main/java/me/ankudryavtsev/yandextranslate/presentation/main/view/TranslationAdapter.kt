package me.ankudryavtsev.yandextranslate.presentation.main.view

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import me.ankudryavtsev.yandextranslate.R
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.Translation

/**
 * @author Kudryavtsev Andrey
 * @date 13/11/2017
 */
class TranslationAdapter(private val translationList: List<Translation>) : RecyclerView.Adapter<TranslationAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindView(translationList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.translation_row_view, parent, false))

    override fun getItemCount(): Int = translationList.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val translationTextView: TextView = itemView.findViewById(R.id.translation_text)

        fun bindView(translation: Translation) {
            translationTextView.text = "${translation.fromText} -> ${translation.toText} [${translation.fromLanguage} -> ${translation.toLanguage}]"
        }
    }
}