package me.ankudryavtsev.yandextranslate.business.translate

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import me.ankudryavtsev.yandextranslate.business.translate.model.TranslateModel
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.repository.translate.ITranslateRepository
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.Translation
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.TranslationDb
import java.util.*
import javax.inject.Inject

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
class TranslateInteractor @Inject constructor(private val translateRepository: ITranslateRepository,
                                              private val translationDb: TranslationDb) : ITranslateInteractor {
    override fun translate(text: String?, fromLanguage: TranslateLanguage, toLanguage: TranslateLanguage): Single<TranslateModel> {
        if (!text.isNullOrEmpty()) {
            return translateRepository.translate(text, fromLanguage, toLanguage)
                    .map {
                        if (it.code != 200)
                            throw WrongResponseCodeException(it.code)
                        else
                            TranslateModel(it.text)
                    }.singleOrError()
        }
        return Single.just(TranslateModel(Collections.emptyList()))
    }

    override fun saveTranslation(fromText: String, fromLanguage: TranslateLanguage, toText: String, toLanguage: TranslateLanguage): Completable =
            Completable.fromAction {
                val translationDao = translationDb.translationDao()
                translationDao.addTranslation(Translation(fromText, toText, fromLanguage, toLanguage))
            }

    override fun getSavedTranslations(): Flowable<List<Translation>> = translationDb.translationDao().getAllTranslations()
}