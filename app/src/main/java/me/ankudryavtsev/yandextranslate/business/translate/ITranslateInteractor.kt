package me.ankudryavtsev.yandextranslate.business.translate

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import me.ankudryavtsev.yandextranslate.business.translate.model.TranslateModel
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.Translation

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
interface ITranslateInteractor {

    fun translate(text: String?, fromLanguage: TranslateLanguage, toLanguage: TranslateLanguage): Single<TranslateModel>

    fun saveTranslation(fromText: String, fromLanguage: TranslateLanguage, toText: String, toLanguage: TranslateLanguage): Completable

    fun getSavedTranslations(): Flowable<List<Translation>>
}