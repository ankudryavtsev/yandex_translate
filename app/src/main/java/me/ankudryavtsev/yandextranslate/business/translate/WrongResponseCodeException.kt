package me.ankudryavtsev.yandextranslate.business.translate

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
class WrongResponseCodeException(val code: Int) : Exception("Response code: $code != 200")