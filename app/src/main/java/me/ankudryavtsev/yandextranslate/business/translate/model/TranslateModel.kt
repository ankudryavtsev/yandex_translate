package me.ankudryavtsev.yandextranslate.business.translate.model

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
data class TranslateModel(val text: List<String>)