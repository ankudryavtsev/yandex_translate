package me.ankudryavtsev.yandextranslate.business.translate

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import me.ankudryavtsev.yandextranslate.business.translate.model.TranslateModel
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.data.network.models.translate.response.TranslateResponse
import me.ankudryavtsev.yandextranslate.data.repository.translate.ITranslateRepository
import me.ankudryavtsev.yandextranslate.data.repository.translate.model.TranslationDb
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString

/**
 * Created by Andrey Kudryavtsev on 14.08.17.
 */
class TranslateInteractorTest {

    lateinit var translateRepository: ITranslateRepository
    lateinit var translateInteractor: ITranslateInteractor
    lateinit var translateDb: TranslationDb

    @Before
    fun setUp() {
        translateRepository = mock()
        translateDb = mock()
        translateInteractor = TranslateInteractor(translateRepository, translateDb)
    }

    @Test
    fun translate_allSuccess() {
        val translateResult = listOf("лол", "кек")
        whenever(translateRepository.translate(anyString(), any(), any()))
                .thenReturn(Observable.fromCallable { TranslateResponse(200, "en-ru", translateResult) })
        val testObserver = TestObserver.create<TranslateModel>()
        translateInteractor.translate("lol", TranslateLanguage.EN, TranslateLanguage.RU).subscribe(testObserver)
        //verify(lol)
        testObserver.awaitTerminalEvent()
        // test no errors was not occurred
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        val translateModel = testObserver.values()[0]
        assertThat(translateModel.text, `is`(translateResult))
    }

    @Test
    fun translate_wrongCodeError() {
        val translateResult = listOf("лол", "кек")
        whenever(translateRepository.translate(anyString(), any(), any()))
                .thenReturn(Observable.fromCallable { TranslateResponse(201, "en-ru", translateResult) })
        val testObserver = TestObserver.create<TranslateModel>()
        translateInteractor.translate("lol", TranslateLanguage.EN, TranslateLanguage.RU).subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        testObserver.assertError(WrongResponseCodeException::class.java)
    }

    @Test
    fun translate_otherError() {
        whenever(translateRepository.translate(anyString(), any(), any()))
                .thenReturn(Observable.error(RuntimeException()))
        val testObserver = TestObserver.create<TranslateModel>()
        translateInteractor.translate("lol", TranslateLanguage.EN, TranslateLanguage.RU).subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        testObserver.assertError(RuntimeException::class.java)
    }

    @Test
    fun translate_null() {
        whenever(translateRepository.translate(anyString(), any(), any()))
                .thenReturn(Observable.error(RuntimeException()))
        val testObserver = TestObserver.create<TranslateModel>()
        translateInteractor.translate(null, TranslateLanguage.ES, TranslateLanguage.RU).subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        val translateModel = testObserver.values()[0]
        assertThat(translateModel.text.isEmpty(), `is`(true))
    }

    @Test
    fun translate_empty() {
        whenever(translateRepository.translate(anyString(), any(), any()))
                .thenReturn(Observable.error(RuntimeException()))
        val testObserver = TestObserver.create<TranslateModel>()
        translateInteractor.translate("", TranslateLanguage.EN, TranslateLanguage.ES).subscribe(testObserver)
        testObserver.awaitTerminalEvent()
        val translateModel = testObserver.values()[0]
        assertThat(translateModel.text.isEmpty(), `is`(true))
    }

}