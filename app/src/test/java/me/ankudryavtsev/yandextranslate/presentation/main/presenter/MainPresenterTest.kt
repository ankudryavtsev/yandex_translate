package me.ankudryavtsev.yandextranslate.presentation.main.presenter

import com.nhaarman.mockito_kotlin.*
import io.reactivex.Flowable
import io.reactivex.Single
import me.ankudryavtsev.yandextranslate.business.translate.ITranslateInteractor
import me.ankudryavtsev.yandextranslate.business.translate.model.TranslateModel
import me.ankudryavtsev.yandextranslate.data.network.models.translate.TranslateLanguage
import me.ankudryavtsev.yandextranslate.presentation.main.view.IMainView
import me.ankudryavtsev.yandextranslate.utils.rx.RxSchedulersTest
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers

/**
 * @author Kudryavtsev Andrey
 * @date 11.09.17
 */
class MainPresenterTest {

    private var translateInteractor: ITranslateInteractor? = null
    private var mainPresenter: MainPresenter? = null
    private var mainView: IMainView? = null

    @Before
    fun before() {
        translateInteractor = mock()
        val rxSchedulersTest = RxSchedulersTest()
        mainPresenter = MainPresenter(translateInteractor!!, rxSchedulersTest)
        mainView = mock()
    }

    @Test
    fun onTextChanged() {
        val testString = "test"
        val translationResult = TranslateModel(listOf("result"))
        whenever(translateInteractor?.translate(ArgumentMatchers.anyString(), any(), any()))
                .thenReturn(Single.fromCallable { translationResult })
        whenever(translateInteractor?.getSavedTranslations())
                .thenReturn(Flowable.empty())
        mainPresenter?.attachView(mainView!!)
        mainPresenter?.fromLanguageSelected(TranslateLanguage.ES)
        mainPresenter?.toLanguageSelected(TranslateLanguage.RU)
        mainPresenter?.onTextChanged(testString)
        verify(mainView, never())?.showError(ArgumentMatchers.anyString())
        verify(mainView, times(1))?.showTranslationResult("result")
    }
}